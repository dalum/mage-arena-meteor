import { Meteor } from 'meteor/meteor';
import '../imports/api/players.js';
import '../imports/logic/gameloop.js';

Meteor.startup(() => {
    // code to run on server at startup
});
