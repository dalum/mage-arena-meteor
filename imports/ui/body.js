import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

import THREE from 'three';

import {Players} from '../api/players.js';
import {extend} from '../utils/extend.js';
import {snap_closest, move_advance} from '../logic/move.js';

import './body.html';

let player_id = -1;

const renderer = new THREE.WebGLRenderer();

let WIDTH = window.innerWidth;
let HEIGHT = window.innerHeight - 200;
let ASPECT = WIDTH / HEIGHT;

let map = {width: 20, height: 20};

renderer.setSize(WIDTH, HEIGHT);
renderer.setClearColor(0x333F47, 1);

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(30, ASPECT, 0.1, 1000);
camera.position.z = 20;

renderer.domElement.addEventListener('contextmenu', function (event) {
    event.preventDefault();
}, false);

const keycode_dictionary = Keypress._keycode_dictionary;
const keylistener = new Keypress.Listener();

let direction_stack = [];

function register_move(key, direction) {
    keylistener.register_combo({
        "keys": key,
        "on_keydown": function () {
            direction_stack.push(direction);
            Meteor.call('players.move', player_id, direction);
        },
        "on_keyup": function () {
            direction_stack = _.without(direction_stack, direction);
            console.log(direction_stack);
            if (direction_stack.length)
                Meteor.call('players.move', player_id, _.last(direction_stack));
            else
                Meteor.call('players.stop', player_id);
        },
        "prevent_default": true,
        "prevent_repeat": true
    });
}

register_move('up', 'UP');
register_move('down', 'DOWN');
register_move('left', 'LEFT');
register_move('right', 'RIGHT');

// renderer.domElement.addEventListener('mousedown', function (event) {
//     event.preventDefault();

//     if (event.buttons == 2) {
//         let element = event.target;
//         let mouse = new THREE.Vector2(((event.clientX / WIDTH) * 2 - 1),
//                                       (-(event.clientY / HEIGHT) * 2 + 1));
//         let raycaster = new THREE.Raycaster();
//         raycaster.setFromCamera(mouse, camera);
//         let ray = raycaster.ray;
//         let coord = {x: parseInt(camera.position.x - ray.origin.z * ray.direction.x / ray.direction.z),
//                      y: parseInt(camera.position.y - ray.origin.z * ray.direction.y / ray.direction.z)};

//         Meteor.call('players.setTarget', player_id, coord);
//     }
// }, false);


Template.body.onRendered(function () {
    this.find('#gamewindow').appendChild(renderer.domElement);

    function render(timestamp) {
        requestAnimationFrame(render);
        renderer.render(scene, camera);
    }
    render();

    var light = new THREE.PointLight(0xffffff);
    light.position.set(50, 50, 50);
    scene.add(light);
    light = new THREE.PointLight(0xffffff);
    light.position.set(50, -50, 50);
    scene.add(light);

    var geometry = new THREE.PlaneGeometry( map.width, map.height, 0 );
    var material = new THREE.MeshStandardMaterial( {color: 0xff0000} );
    var plane = new THREE.Mesh( geometry, material );
    scene.add( plane );
});

Template.body.helpers({
    players() {
        return Players.find({});
    }
});

Template.body.events({
    'submit .new-player'(event) {
        event.preventDefault();

        const target = event.target;
        const owner = target.owner.value;

        Meteor.call('players.insert', owner);

        target.owner.value = '';
    }
});

Template.player.onRendered(function () {
    this.geometry = new THREE.BoxGeometry(1, 1, 1);
    this.material = new THREE.MeshStandardMaterial({color: this.data.color});
    this.model = new THREE.Mesh(this.geometry, this.material);

    player_id = this.data._id;

    scene.add(this.model);

    extend(this.model.position, Players.findOne(this.data._id, {fields: {position: 1}}).position);

    let this_ = this;
    let model = this.model;

    function bind(property, method=null) {
        let fields = {};
        fields[property] = 1;

        this_.autorun(function () {
            if (method == 'extend')
                extend(this_.model[property],
                       Players.findOne(this_.data._id, {fields: fields})[property]);
            else
                model[property] = Players.findOne(this_.data._id, {fields: fields})[property];
        });
    }

    //bind('position', 'extend');
    bind('target');
    //bind('moving');
    bind('speed');

    requestAnimationFrame(function (timestamp) {this_.t = timestamp; animate(this_, timestamp);});
    function animate(this_, timestamp) {
        requestAnimationFrame(function (timestamp) {animate(this_, timestamp);});
        let dt = (timestamp - this_.t) / 1000;
        this_.t = timestamp;

        extend(model.position, move_advance(model.position,
                                            model.target,
                                            model.speed,
                                            dt));

        if (player_id == this_.data._id) {
            camera.position.x = model.position.x;//  - 5 *
            // (2 * (map.width / 2 - self.cube.position.x) / map.width - 1);
            camera.position.y = model.position.y - 5;//  - 5 - 5 *
            // (2 * (map.height / 2 - self.cube.position.y) / map.height - 1);
            camera.lookAt(new THREE.Vector3(model.position.x,
                                            model.position.y,
                                            0));
        }
    }
});
