export const KEYS = {UP: 38, DOWN: 40, LEFT: 37, RIGHT: 39};

export let keystates = {};

let keyhooks = {};

function onKeyDown(event) {
    keystates[event.keyCode] = true;
    
}

function onKeyUp(event) {
    keystates[event.keyCode] = false;
}

window.addEventListener('keydown', onKeyDown, false);
window.addEventListener('keyup', onKeyUp, false);

export function key_down_register(keyCode, fn) {
    if (!(keyCode in keyhooks)) {
        keyhooks[keyCode] = [fn];
    }
    else {
        keyhooks[keyCode].push(fn);
    }
}
