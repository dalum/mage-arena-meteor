export const DIRECTIONS = {NONE: {x: 0, y: 0},
                           UP: {x: 0, y: 1},
                           DOWN: {x: 0, y: -1},
                           LEFT: {x: -1, y: 0},
                           RIGHT: {x: 1, y: 0}};

export function snap_closest(origin, displacement=DIRECTIONS.NONE) {
    let point = {};
    for (let x in displacement) {
        let o = origin[x];
        let d = displacement[x];
        point[x] = Math.round(o + d * (1 - d * o + Math.floor(d * o)));
    }
    return point;
}

export function move_advance(position, target, speed, dt) {
    let distance = {x: Math.abs(target.x - position.x),
                    y: Math.abs(target.y - position.y)};
    let direction = {
        x: (distance.x && (!distance.y || distance.x <= distance.y)) ? (target.x - position.x) / distance.x : 0,
        y: (distance.y && (!distance.x || distance.y < distance.x)) ? (target.y - position.y) / distance.y : 0
    };

    return {x: position.x + direction.x * Math.min(speed * dt, distance.x),
            y: position.y + direction.y * Math.min(speed * dt, distance.y)};
}
