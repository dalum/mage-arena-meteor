import {extend} from '../utils/extend.js';

import {Players} from '../api/players.js';

import {DIRECTIONS, snap_closest, move_advance} from './move.js';

let players_cursor = Players.find({});

let timestamp = Date.now();

loop();
function loop() {
    let dt = (Date.now() - timestamp) / 1000;
    timestamp = Date.now();

    players_cursor.forEach(function (player) {
        let target = player.target;
        if (player.moving)
            target = snap_closest(player.position, player.move_direction);
        else
            target = snap_closest(player.position);

        target.x = Math.max(-10, target.x);
        target.x = Math.min(10, target.x);
        target.y = Math.max(-10, target.y);
        target.y = Math.min(10, target.y);
        Players.update(player._id, {$set: {
            position: extend(player.position, move_advance(player.position, target,
                                                           player.speed, dt)),
            target: target
        }});
    });

    Meteor.setTimeout(loop, 10);
}
