import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';
import {check} from 'meteor/check';

import {DIRECTIONS} from '../logic/move.js';

export const Players = new Mongo.Collection('players');

Meteor.methods({
    'players.insert'(owner) {
        check(owner, String);

        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        Players.insert({
            owner: owner,

            position: {x: 0, y: 0},
            target: {x: 0, y: 0},
            look_direction: DIRECTIONS.UP,

            move_direction: DIRECTIONS.UP,
            moving: false,
            speed: 5,

            color: 0xFFFF00
        });
    },

    'players.remove'(playerId) {
        check(playerId, String);

        Players.remove(playerId);
    },

    'players.move'(playerId, direction) {
        check(playerId, String);
        check(direction, String);

        if (direction in DIRECTIONS)
            Players.update(playerId, {$set: {move_direction: DIRECTIONS[direction],
                                             moving: true}});
        else
            throw new Meteor.Error('invalid direction');
    },

    'players.stop'(playerId) {
        check(playerId, String);

        Players.update(playerId, {$set: {moving: false}});
    }
});
